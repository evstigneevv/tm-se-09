package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.ITaskRepository;
import ru.evstigneev.tm.api.ITaskService;
import ru.evstigneev.tm.comparator.ComparatorTaskDateCreation;
import ru.evstigneev.tm.comparator.ComparatorTaskDateFinish;
import ru.evstigneev.tm.comparator.ComparatorTaskDateStart;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.*;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskName) throws EmptyStringException {
        if (taskName.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.create(userId, projectId, taskName);
    }

    @Override
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) throws EmptyStringException {
        if (taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.remove(userId, taskId);
    }

    @Override
    public Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.getTaskListByProjectId(userId, projectId);
    }

    @Override
    public Task update(@NotNull final String userId, @NotNull final String taskId, @NotNull final String taskName) throws EmptyStringException {
        if (taskId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.update(userId, taskId, taskName);
    }

    @Override
    public Task merge(@NotNull final String userId, @NotNull final Task task) throws Exception {
        return taskRepository.merge(userId, task);
    }

    @Override
    public Task persist(@NotNull final String userId, @NotNull final Task task) throws Exception {
        return taskRepository.persist(userId, task);
    }

    @Override
    public void removeAll() {
        taskRepository.removeAll();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) {
        return taskRepository.removeAllByUserId(userId);
    }

    @Override
    public boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) {
        return taskRepository.deleteAllProjectTasks(userId, projectId);
    }

    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws EmptyStringException {
        if (taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.findOne(userId, projectId, taskId);
    }

    public List<Task> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull List<Task> sortedList = new ArrayList<>(findAll());
        @Nullable Comparator<Task> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<Task> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case "date start": {
                return new ComparatorTaskDateStart();
            }
            case "date finish": {
                return new ComparatorTaskDateFinish();
            }
            case "date creation": {
                return new ComparatorTaskDateCreation();
            }
            case "status": {
                return null;
            }
        }
    }

    public List<Task> searchByString(@NotNull final String string) throws Exception {
        List<Task> projectList = new ArrayList<>();
        for (Task t : findAll()) {
            if (t.getName().contains(string) || (t.getDescription() != null && t.getDescription().contains(string))) {
                projectList.add(t);
            }
        }
        return projectList;
    }

}
