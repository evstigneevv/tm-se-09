package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.IUserRepository;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;
import java.util.UUID;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setId(UUID.randomUUID().toString());
        user.setRole(RoleType.ADMIN);
        entities.put(user.getId(), user);
        return user;
    }

    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) {
        User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setId(UUID.randomUUID().toString());
        user.setRole(role);
        entities.put(user.getId(), user);
        return user;
    }

    @Override
    public boolean updatePassword(@NotNull final String login, @NotNull final String newPassword) {
        @Nullable final Collection<User> userCollection = entities.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                entities.get(u.getId()).setPasswordHash(newPassword);
                return true;
            }
        }
        return false;
    }

    @Override
    public User findByLogin(@NotNull final String login) {
        @Nullable final Collection<User> userCollection = entities.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                return u;
            }
        }
        return null;
    }

    @Override
    public Collection<User> findAll() {
        return entities.values();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String userName) {
        entities.get(userId).setLogin(userName);
    }

    @Override
    public boolean remove(@NotNull final String userId) {
        return entities.remove(userId) != null;
    }

    @Override
    public User merge(@NotNull final String userId, @NotNull final User user) {
        if (entities.get(userId).getRole().equals(RoleType.ADMIN)) {
            return entities.put(userId, user);
        }
        return null;
    }

    @Override
    public User persist(@NotNull final String userId, @NotNull final User user) {
        if (entities.get(userId).getRole().equals(RoleType.ADMIN)) {
            return entities.put(userId, user);
        }
        return null;
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

}