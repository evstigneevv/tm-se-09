package ru.evstigneev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserRepository {

    User create(@NotNull final String login, @NotNull final String password);

    User create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role);

    boolean updatePassword(@NotNull final String login, @NotNull final String newPassword);

    User findByLogin(@NotNull final String login);

    Collection<User> findAll();

    void update(@NotNull final String userId, @NotNull final String userName);

    boolean remove(@NotNull final String userId);

    User persist(@NotNull final String userId, @NotNull final User user);

    User merge(@NotNull final String userId, @NotNull final User user);

    void removeAll();

}
