package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;

public class ProjectSearchByString extends AbstractCommand {

    @Override
    public String command() {
        return "SPS";
    }

    @Override
    public String description() {
        return "Search projects by string";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter a string for search: ");
        String searchingString = bootstrap.getScanner().nextLine();
        for (Project p : bootstrap.getProjectService().searchByString(searchingString)) {
            System.out.println(p.getName() + p.getDescription() + bootstrap.getDateParser().getDateString(p.getDateStart())
                    + p.getStatus() + bootstrap.getDateParser().getDateString(p.getDateFinish()) + p.getDateOfCreation()
                    + p.getUserId() + p.getId());
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
