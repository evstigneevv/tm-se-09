package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.RoleType;

public class TaskGetByUserCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SUT";
    }

    @Override
    public String description() {
        return "Show all user tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter user ID: ");
        @NotNull final String input = bootstrap.getScanner().nextLine();
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            task ID                  |               task name             |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        for (Task t : bootstrap.getTaskService().findAllByUserId(input)) {
            System.out.println("|" + t.getProjectId() + " |" + t.getId() + " | " + t.getName());
            System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
